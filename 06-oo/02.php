<?php
  require_once("configuracion/bd.php");
  require_once("clases/bd.php");
  require_once("clases/02.php");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
  </head>
  <body>

<?php
  $libro = new Libro($_GET['isbn']);
  if (!$libro->cargarDatos()) {
    /* En caso de error durante la carga de datos desde la BD del libro
     * Mostramos el mensaje de error que se haya generado.
     */ 
?>
  <h1><?php echo $libro->error; ?></h1>
<?php
  } else {
    /* En caso contrario mostramos los datos del libro */
?>
    <h1>Datos del libro</h1>
    <h3>ISBN: <?php echo $libro->getIsbn(); ?></h3>
    <h3>Titulo: <?php echo $libro->getTituloLibro(); ?></h3>
    <h3>Ejemplares:</h3>
<?php
    foreach ($libro->getEjemplares() as $ejemplar) {
?>
      <h4><?php echo $ejemplar['clave_ejemplar']; ?> <?php echo $ejemplar['conservacion_ejemplar']; ?></h4>
<?php
    }
?>
    <h3>Autores:</h3>
<?php
    foreach ($libro->getAutores() as $autor) {
?>
      <h4><?php echo $autor['nombre_autor']; ?></h4>
<?php
    }
  }
?>

  <hr />
  <pre>
  ********************************
  ** Estado el objeto $libro **

<?php
  var_dump($libro);
?>

  ********************************
  </pre>

  <ul>Pruebas
    <li><a href="02.php?isbn=">02.php?isbn=</a></li>
    <li><a href="02.php?isbn=12345">02.php?isbn=12345</a></li>
    <li><a href="02.php?isbn=123456789x">02.php?isbn=123456789x</a></li>
    <li><a href="02.php?isbn=1234567890">02.php?isbn=1234567890</a></li>
    <li><a href="02.php?isbn=6875362773">02.php?isbn=6875362773</a></li>
    <li><a href="02.php?isbn=5221695172">02.php?isbn=5221695172</a></li>
    <li><a href="02.php?isbn=1827617995">02.php?isbn=1827617995</a></li>
  </ul>
  </body>
</html>
